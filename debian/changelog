libcps-perl (0.19-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.4.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 30 Jun 2022 09:28:15 +0100

libcps-perl (0.19-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.19.
  * Add deprecation warning to debian/control.
  * Update years of upstream and packaging copyright.
  * Annotate test-only build dependencies with <!nocheck>.
  * Declare compliance with Debian Policy 4.4.0.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Tue, 23 Jul 2019 18:46:29 -0300

libcps-perl (0.18-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * New upstream release.
  * Bump Standards-Version to 3.9.4 (no changes).
  * Add (build) dependency on libfuture-perl.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Axel Beckert ]
  * Mark package as autopkgtestable
  * Declare compliance with Debian Policy 3.9.6
  * Add explicit build dependency on libmodule-build-perl

 -- Axel Beckert <abe@debian.org>  Sun, 07 Jun 2015 18:57:41 +0200

libcps-perl (0.17-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sat, 25 Aug 2012 21:46:47 +0200

libcps-perl (0.16-1) unstable; urgency=low

  * Team upload.
  * New upstream release.

 -- Nuno Carvalho <smash@cpan.org>  Tue, 24 Jul 2012 21:44:06 +0100

libcps-perl (0.15-1) unstable; urgency=low

  * New upstream release.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Wed, 27 Jun 2012 18:10:54 +0200

libcps-perl (0.14-1) unstable; urgency=low

  * Imported Upstream version 0.14
  * Bump Standards-Version to 3.9.3
  * Update Format url using copyright-format 1.0
  * Remove no more necessary spelling.patch: it's included in source code
  * Sort B-D-I

 -- Fabrizio Regalli <fabreg@fabreg.it>  Thu, 01 Mar 2012 23:58:08 +0100

libcps-perl (0.13-1) unstable; urgency=low

  * Imported Upstream version 0.13
  * Added myself to Uploaders and Copyright
  * Updated copyright with latest DEP5 (rev .174) format
  * Updated copyright year

 -- Fabrizio Regalli <fabreg@fabreg.it>  Thu, 16 Feb 2012 18:12:59 +0100

libcps-perl (0.12-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * New upstream release.
  * Bump Standards-Version to 3.9.2 (no changes).
  * Add libtest-identity-perl, libtest-refcount-perl, and libtest-fatal-
    perl to Build-Depends-Indep.
  * Add /me to Uploaders.
  * Update year of upstream copyright.
  * Add a patch to fix a spelling mistake.

 -- gregor herrmann <gregoa@debian.org>  Wed, 28 Dec 2011 00:04:15 +0100

libcps-perl (0.11-1) unstable; urgency=low

  * Initial Release (Closes: #608560)

 -- Jonathan Yu <jawnsy@cpan.org>  Sat, 01 Jan 2011 09:25:17 -0500
